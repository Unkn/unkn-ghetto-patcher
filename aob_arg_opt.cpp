/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "aob_arg_opt.h"
#include "Logger.h"

bool aob_arg_opt::parse_impl(const std::vector<const char *> & args)
{
    bool ret = false;

    if (parsed())
        LOG_DEBUG("aob has already been set");
    else if (args.size() != 1)
        LOG_DEBUG("Invalid number of args for aob option: {:d}", args.size());
    else if ((aob_ = unkn::input_to_aob(args.front())).size() == 0)
        LOG_DEBUG("Failed to parse input as aob");
    else
        ret = true;

    return ret;
}
