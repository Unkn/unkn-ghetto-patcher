/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef AOB_ARG_OPT_H
#define AOB_ARG_OPT_H

#include <vector>

#include "ArgOpt.h"
#include "hex_utils.h"

class aob_arg_opt : public ArgOpt
{
    public:
        aob_arg_opt(char shortName, std::string desc) :
            ArgOpt(shortName, REQUIRED_ARGUMENT, desc) {}
        aob_arg_opt(const std::string & longName, std::string desc) :
            ArgOpt(longName, REQUIRED_ARGUMENT, desc) {}
        aob_arg_opt(char shortName, const std::string & longName, std::string desc) :
            ArgOpt(shortName, longName, REQUIRED_ARGUMENT, desc) {}

        virtual ~aob_arg_opt() {}

        const auto & aob() const { return aob_; }

    protected:
        bool parse_impl(const std::vector<const char *> & args) override;

    private:
        array_of_bytes aob_;
};

#endif
