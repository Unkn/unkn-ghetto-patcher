/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FILE_PARSER_H
#define FILE_PARSER_H

#include <fstream>
#include <string>

#include "stream_parser.h"
#include "Logger.h"

class file_parser : public stream_parser
{
    public:
        virtual ~file_parser() { }

    protected:
        // To expose these to public scope in a child:
        //
        //    using file_parser::load;
        //    using file_parser::save;
        //
        // Only the implemented functions should be exposed.

        bool load(const std::string & in)
        {
            bool ret = false;
            std::ifstream ifs(in);
            if (!ifs)
                LOG_ERROR("Failed to open file: {}", in);
            else if (!read(ifs))
                LOG_ERROR("Failed to load from file: {}", in);
            else
            {
                LOG_INFO("Successfully loaded from file: {}", in);
                ret = true;
            }
            return ret;
        }

        bool save(const std::string & out)
        {
            bool ret = false;
            std::ofstream ofs(out);
            if (!ofs)
                LOG_ERROR("Failed to open file: {}", out);
            else if (!write(ofs))
                LOG_ERROR("Failed to save to file: {}", out);
            else
            {
                LOG_INFO("Successfully saved to file: {}", out);
                ret = true;
            }
            return ret;
        }
};

#endif
