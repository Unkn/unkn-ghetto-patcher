/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "file_patcher.h"

#include "Logger.h"

template<class...Ts> struct overloaded : Ts... { using Ts::operator()...; };
template<class...Ts> overloaded(Ts...) -> overloaded<Ts...>;

file_patcher::file_patcher(
        const std::string & file,
        const std::string & gen_revert,
        const std::string & gen_direct,
        const std::string & load) :
    patcher(file, gen_revert, gen_direct),
    load_(load)
{
}

file_patcher::~file_patcher()
{
}

bool file_patcher::run()
{
    bool ret = true;
    patch_file_parser pfp;
    overloaded visitor
    {
        [](const auto &)
        {
            LOG_ERROR("Invalid file patch type");
            return false;
        },
        // TODO Investigate how these captures works becuase it seems to be
        // engrained in the lambda instead of imposing state like a classic
        // struct style functor. Was surprised this worked.
        [this](const patch_file_parser::direct_patch & dp) { return apply_patch(dp); },
        [this](const patch_tree & pt) { return apply_patch(pt); }
    };

    if (!pfp.load(load_))
    {
        LOG_ERROR("Failed to load patch file '{}'", load_);
        ret = false;
    }
    else
    {
        ret = std::visit(visitor, pfp.get_patch());
    }

    return ret;
}

bool file_patcher::apply_patch(
        const patch_file_parser::direct_patch & dp) const
{
    // TODO Check file hash
    return apply_patch(dp.patch_);
}
