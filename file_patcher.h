/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef FILE_PATCHER_H
#define FILE_PATCHER_H

#include "patcher.h"
#include "patch_file_parser.h"

class file_patcher final : public patcher
{
    public:
        file_patcher(
                const std::string & file,
                const std::string & gen_revert,
                const std::string & gen_direct,
                const std::string & load);
        virtual ~file_patcher();

        bool run() override;

    protected:
        using patcher::apply_patch;

    private:
        std::string load_;

        bool apply_patch(const patch_file_parser::direct_patch & dp) const;
};

#endif
