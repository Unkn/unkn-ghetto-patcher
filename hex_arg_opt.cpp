/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "hex_arg_opt.h"

#include "hex_utils.h"
#include "Logger.h"

bool hex_arg_opt::parse_impl(const std::vector<const char *> & args)
{
    bool ret = false;

    if (parsed())
        LOG_DEBUG("hex has already been set");
    else if (args.size() != 1)
        LOG_DEBUG("Invalid number of args for hex option: {:d}", args.size());
    else if ((bin_ = unkn::hex_to_bin(args.front())).size() == 0)
        LOG_DEBUG("Failed to parse input as hex");
    else
        ret = true;

    return ret;
}
