/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HEX_ARG_OPT_H
#define HEX_ARG_OPT_H

#include "ArgOpt.h"

#include <vector>

class hex_arg_opt : public ArgOpt
{
    public:
        hex_arg_opt(char shortName, std::string desc) :
            hex_arg_opt(shortName, "", desc) {}
        hex_arg_opt(const std::string & longName, std::string desc) :
            hex_arg_opt('\0', longName, desc) {}
        hex_arg_opt(char shortName, const std::string & longName, std::string desc) :
            ArgOpt(shortName, longName, REQUIRED_ARGUMENT, desc) {}

        virtual ~hex_arg_opt() {}

        const auto & bin() const { return bin_; }

    protected:
        bool parse_impl(const std::vector<const char *> & args) override;

    private:
        std::vector<char> bin_;
};

#endif
