/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "hex_utils.h"

template<char min, char max, char deduction>
static inline bool convert_nibble_in_range(char & c)
{
    if (c < min || max < c)
        return false;
    c -= deduction;
    return true;
}

static bool nibble_to_bin(char & c)
{
    // An alternative would be to use a switch -> jump table. Possibly fewer
    // instructions at the cost of more space. The gain should be negligible
    // for this though and it takes more lines of code to write.
    //
    // TODO _ACM_ Still would be a tiny bit interesting to compare in callgrind
    return convert_nibble_in_range<'0', '9', '0'>(c) ||
        convert_nibble_in_range<'a', 'f', 'a' - 10>(c) ||
        convert_nibble_in_range<'A', 'F', 'A' - 10>(c);
}

static std::optional<char> hex_to_bin(char upper, char lower)
{
    return (nibble_to_bin(upper) && nibble_to_bin(lower)) ?
        std::optional<char>((upper << 4) | lower) :
        std::optional<char>();
}

namespace unkn
{
    array_of_bytes input_to_aob(
            std::string_view input)
    {
        array_of_bytes aob;
        char upper;
        const auto size = input.size();
        bool ok = true;

        for (auto i = input.find_first_not_of(' ');
                // We could check ok here, but we break out of the loop when ok
                // is false already
                i != std::string_view::npos;
                i = input.find_first_not_of(' ', i))
        {
            std::optional<char> bin;

            if ((upper = input[i++]) == '?')
            {
                // Allow single and double '?'
                if (i != size && input[i] == '?')
                    ++i;
            }
            else if (i == size || !(bin = ::hex_to_bin(upper, input[i++])))
            {
                ok = false;
                break;
            }

            aob.emplace_back(std::move(bin));
        }

        if (!ok)
            aob.clear();

        return aob;
    }

    std::vector<char> hex_to_bin(std::string_view input)
    {
        std::vector<char> bin;
        const auto size = input.size();
        bool ok = true;

        for (auto i = input.find_first_not_of(' ');
                // We could check ok here, but we break out of the loop when ok
                // is false already
                i != std::string_view::npos;
                i = input.find_first_not_of(' ', i + 2))
        {
            std::optional<char> byte;

            if (size <= i + 1 || !(byte = ::hex_to_bin(input[i], input[i + 1])))
            {
                ok = false;
                break;
            }

            bin.emplace_back(byte.value());
        }

        if (!ok)
            bin.clear();

        return bin;
    }
}
