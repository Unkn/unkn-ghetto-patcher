/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef HEX_UTILS_H
#define HEX_UTILS_H

#include <optional>
#include <string_view>
#include <vector>

// TODO Consider reducing this to 'aob' and moving into the unkn namespace;
// would be a lot nicer and its a common acronym for the community that would
// use this
using array_of_bytes = std::vector<std::optional<char> >;

namespace unkn
{
    array_of_bytes input_to_aob(std::string_view input);
    std::vector<char> hex_to_bin(std::string_view input);
}

#endif
