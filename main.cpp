/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <cstdlib>

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>

#include "Logger.h"
#include "StdOutLogMethod.h"
#include "patcher_arg_parser.h"

int main(int argc, char ** argv)
{
    USE_STDOUT_LOG_METHOD;

    std::unique_ptr<patcher> p;
    {
        // No reason to keep the parser around for the lifetime of the program
        p = patcher_arg_parser().parse(argc, argv);
        if (!p) exit(EXIT_FAILURE);
    }
    exit(p->run() ? EXIT_SUCCESS : EXIT_FAILURE);
}
