/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "md5.h"

#include <openssl/md5.h>

// TODO Implement md5 to allow the option to not link against openssl
std::vector<unsigned char> md5(std::istream & is)
{
    char buff[256];
    std::vector<unsigned char> hash;
    MD5_CTX ctx;
    MD5_Init(&ctx);
    while (is)
    {
        is.read(buff, sizeof(buff));
        auto len = is.gcount();
        if (len > 0)
            MD5_Update(&ctx, buff, len);
    }
    if (is.eof())
    {
        hash.resize(md5_length());
        MD5_Final(hash.data(), &ctx);
    }
    return hash;
}
