/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef MD5_H
#define MD5_H

#include <istream>
#include <vector>

constexpr int md5_length() { return 16; }
std::vector<unsigned char> md5(std::istream & is);

#endif
