/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "patch_file_parser.h"

#include <arpa/inet.h> // ntohl

#include <limits>
#include <string_view>

#include "hex_utils.h"
#include "string_utils.h"
#include "md5.h"

#define TYPE_AOB_STR    "aob"
#define TYPE_DIRECT_STR "direct"

void patch_file_parser::clear()
{
    patch_.emplace<std::monostate>();
}

bool patch_file_parser::read(std::istream & is)
{
    clear();
    type t = read_type(is);
    switch (t)
    {
        case type::aob: return read_aob_file(is);
        case type::direct: return read_direct_file(is);
        default: return false;
    }
}

bool patch_file_parser::write(std::ostream & os)
{
    return false;
}

patch_file_parser::type patch_file_parser::read_type(
        std::istream & is) const
{
    type ret = type::none;
    std::string line;
    // TODO read line
    if (line == TYPE_AOB_STR)
        ret = type::aob;
    else if (line == TYPE_DIRECT_STR)
        ret = type::direct;
    return ret;
}

bool patch_file_parser::read_aob_file(
        std::istream & is)
{
    bool ret = true;
    patch_tree pt;
    std::string line;
    // TODO Count the lines processed to help make finding issues in the patch
    // file simpler
    while (ret && next_raw_aob_line(is, line))
    {
        array_of_bytes aob;
        std::vector<char> data;
        int offset = 0;
        char *end;
        auto seg = unkn::split(line, ':', true);

        switch (seg.size())
        {
            case 3:
                // Note that the offset is the last segment so a null
                // terminator will follow it
                offset = strtol(seg[2].data(), &end, 0);
                if (*end != '\0')
                {
                    LOG_ERROR("Failed to parse offset '{}'", seg[2]);
                    ret = false;
                    break;
                }

                // Fallthrough on success
            case 2:
                if ((aob = std::move(unkn::input_to_aob(seg[0]))).empty())
                {
                    LOG_ERROR("Empty AOB provided");
                    ret = false;
                }
                else if ((data = std::move(unkn::hex_to_bin(seg[1]))).empty())
                {
                    LOG_ERROR("Empty patch bytes provided");
                    ret = false;
                }
                else if (!pt.add_patch(aob, std::move(data), offset))
                {
                    LOG_ERROR("Failed to add patch");
                    ret = false;
                }

                LOG_DEBUG("Processed data at file offset 0x{:08X}", is.tellg() - is.gcount());
                break;

            default:
                LOG_ERROR("Invalid number of segments in line");
                ret = false;
                break;
        }

        // TODO Error at file offset message on failure
    }

    if (ret)
    {
        patch_.emplace<patch_tree>(std::move(pt));
    }

    return ret;
}

bool patch_file_parser::next_raw_aob_line(
        std::istream & is,
        std::string & line) const
{
    while (is && line.empty())
    {
        if (is.peek() == '#')
        {
            is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            continue;
        }

        std::getline(is, line, '\n');
        if (auto pos = line.find_first_of('#'); pos != std::string::npos)
            line.erase(pos);
    }
    return !line.empty();
}

bool patch_file_parser::read_direct_file(
        std::istream & is)
{
    bool ret = true;
    direct_patch dp;
    int id = 0;
    if (!read_file_hash(is, dp.hash_))
    {
        LOG_ERROR("Failed to read file hash from file");
        ret = false;
    }
    else
    {
        while (ret && is)
        {
            int pos;
            std::vector<char> data;
            if (!read_patch_segment(is, pos, data))
            {
                LOG_ERROR("Failed to read patch segment from file");
                ret = false;
            }
            else if (!dp.patch_.emplace(std::piecewise_construct,
                        std::forward_as_tuple(pos),
                        std::forward_as_tuple(
                            std::make_shared<std::vector<char> >(std::move(data)),
                            id))
                    .second)
            {
                LOG_ERROR("Failed to add parsed patch segment {} to patch", id);
                ret = false;
            }
            else
            {
                LOG_INFO("Added patch segment {} to patch", id++);
            }
        }

        // I Don't think we will ever hit this case because of the behavior of
        // the loop... but just in case :|
        if (ret && !is.eof())
        {
            LOG_ERROR("Failed to parse complete patch file");
            ret = false;
        }
    }
    return ret;
}

bool patch_file_parser::read_file_hash(
        std::istream & is,
        std::vector<unsigned char> & hash) const
{
    // NOTE: We use md5 because it's fast and the hash is not being used for
    // security purposes, just a safeguard against accidentally patching the
    // wrong file.
    hash.resize(md5_length());
    return is.read(reinterpret_cast<char *>(hash.data()), md5_length()) && is.gcount() == md5_length();
}

bool patch_file_parser::read_patch_segment(
        std::istream & is,
        int & pos,
        std::vector<char> & data) const
{
    bool ret = false;
    int data_len;
    if (!read_int(is, pos))
        LOG_ERROR("Failed to read patch offset");
    else if (!read_int(is, data_len))
        LOG_ERROR("Failed to read patch length");
    else if (!read_patch_data(is, data, data_len))
        LOG_ERROR("Failed to read patch data");
    else
        ret = true;
    return ret;
}

bool patch_file_parser::read_int(
        std::istream & is,
        int & i) const
{
    if (!is.read(reinterpret_cast<char *>(&i), sizeof(i)) || is.gcount() != sizeof(i))
    {
        LOG_ERROR("Failed to read int from file");
        return false;
    }
    i = ntohl(i);
    LOG_DEBUG("Read int: {}", i);
    return true;
}

bool patch_file_parser::read_patch_data(
        std::istream & is,
        std::vector<char> & data,
        int data_len) const
{
    data.resize(data_len);
    return is.read(data.data(), data_len) && is.gcount() == data_len;
}
