/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PATCH_FILE_PARSER_H
#define PATCH_FILE_PARSER_H

#include "file_parser.h"
#include "patch_tree.h"

class patch_file_parser : public file_parser
{
    public:
        using file_parser::load;
        using file_parser::save;

        // TODO Provide access methods for the patch_tree and direct_patch
        // which throws when the type is incorrect for the accessor
        void clear();

        struct direct_patch
        {
            ::patch patch_;
            std::vector<unsigned char> hash_;
        };
        using patch = std::variant<std::monostate, patch_tree, direct_patch>;

        patch & get_patch() { return patch_; };
        const patch & get_patch() const { return patch_; };

    protected:
        bool read(std::istream & is) override;
        bool write(std::ostream & os) override;

    private:
        enum type
        {
            none,
            aob,
            direct
        };

        patch patch_;

        type read_type(std::istream & is) const;
        bool read_aob_file(std::istream & is);
        bool next_raw_aob_line(std::istream & is, std::string & line) const;

        bool read_direct_file(std::istream & is);
        bool read_file_hash(std::istream & is, std::vector<unsigned char> & hash) const;
        bool read_patch_segment(std::istream & is, int & pos, std::vector<char> & data) const;
        bool read_int(std::istream & is, int & i) const;
        bool read_patch_data(std::istream & is, std::vector<char> & data, int data_len) const;
};

#endif
