/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "patch_tree.h"

#include <cmath>

#include "Logger.h"

bool patch_tree::add_patch(
        const array_of_bytes & aob,
        std::vector<char> && data,
        std::istream::off_type off)
{
    if (aob.size() < 1)
    {
        LOG_ERROR("Cannot have a patch with an AOB of zero length");
        return false;
    }

    if (!aob.front() || !aob.back())
    {
        LOG_ERROR("AOBs cannot begin or end with a wildcard");
        return false;
    }

    node * c = &roots_[aob.front().value()];
    aob_node * tmp;
    size_t i = 1;

    while ((tmp = std::get_if<aob_node>(c)) && i < aob.size())
        c = &next_node(aob[i++], *tmp);

    // If tmp is null, then c must be a patch_id.
    // if tmp is not empty, then there exists a patch where the current aob
    // is subsequence of another aob.
    //
    // TODO We will not catch wildcard aob's that will match a specific aob,
    // however a more specific one with respect to leicographical ordering will
    // have a matching priority. Think about a nice way to warn about overlaps
    // from the time of constructing the aob's.
    if (!tmp || !tmp->empty())
    {
        LOG_ERROR("AOB for patch conflicts with existing AOB");
        return false;
    }

    patch_id next = patches_.size();
    c->emplace<patch_id>(next);

    auto p = patches_.emplace_back();
    p.data_ = std::make_shared<std::vector<char> >(std::move(data));
    p.off_ = off;

    LOG_INFO("Inserted new patch with id {}", next);

    return true;
}

std::optional<patch> patch_tree::generate_patch(
        std::istream & is) const
{
    bool ok = true;
    patch ret;
    std::set<patch_id> match_ids;

    for (const auto & tree : roots_)
    {
        is.clear();
        is.seekg(0);

        const auto ignore_max = std::numeric_limits<std::streamsize>::max();
        const char first = tree.first;
        const node & root = tree.second;

        while (is.ignore(ignore_max, first))
        {
            auto pos = is.tellg();
            auto matches = find_matches(root, is);
            for (auto & match : matches)
            {
                auto & raw = patches_[match];

                if (raw.off_ < 0 && pos < static_cast<decltype(pos)>(std::abs(raw.off_)))
                {
                    LOG_ERROR("Match for patch {} found at 0x{:08X} but has offset -0x{:08X}",
                            match, pos, std::abs(raw.off_));
                    ok = false;
                    // If the position is invalid then this patch is just inapplicable
                    continue;
                }

                if (!match_ids.emplace(match).second)
                {
                    LOG_ERROR("Match for patch {} found again at 0x{:08X}", is.tellg() + raw.off_);
                    ok = false;
                }

                auto er = ret.emplace(std::piecewise_construct,
                        std::forward_as_tuple(pos + raw.off_),
                        std::forward_as_tuple(raw.data_, match));
                if (!er.second)
                {
                    LOG_ERROR("Conflicting patch found at 0x{:08X}: {}, {}",
                            pos + raw.off_, match, er.first->second.id_);
                    ok = false;
                }
                else
                {
                    LOG_INFO("Found match for patch {} at 0x{:08X}", match, pos);
                }
            }
        }
    }

    // TODO if ok, do a pass on the patches found to check for overlaps on the
    // patches to be applied

    return ok ? std::optional<patch>(std::move(ret)) : std::optional<patch>();
}

patch_tree::node & patch_tree::next_node(
        const std::optional<char> & b,
        aob_node & n)
{
    if (b)
    {
        // The node will get default constructed as an aob_node if no node
        // exists for the given value
        return n.next_[b.value()];
    }
    else
    {
        // If there is no wildcard initialized yet, then do so now. The node
        // will be initialized as an aob_node.
        if (!n.wildcard_)
            n.wildcard_ = std::make_unique<node>();
        return *n.wildcard_;
    }
}

std::set<patch_tree::patch_id> patch_tree::find_matches(const node & n, std::istream & is) const
{
    std::set<patch_id> matches;
    find_matches(n, is, matches);
    return matches;
}

void patch_tree::find_matches(const node & n, std::istream & is, std::set<patch_id> & matches) const
{
    if (auto id = std::get_if<patch_id>(&n))
    {
        matches.emplace(*id);
    }
    else
    {
        auto & an = std::get<aob_node>(n);
        auto pos = is.tellg();
        char c;
        if (is.get(c))
        {
            if (auto it = an.next_.find(c); it != an.next_.end())
            {
                auto & var = it->second;
                if (auto id = std::get_if<patch_id>(&var))
                    matches.emplace(*id);
                else
                    find_matches(var, is, matches);
            }

            if (an.wildcard_)
            {
                find_matches(*an.wildcard_, is, matches);
            }
        }
        else
        {
            is.clear();
        }
        is.seekg(pos);
    }
}
