/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PATCH_TREE_H
#define PATCH_TREE_H

#include <istream>
#include <map>
#include <memory>
#include <set>
#include <variant>
#include <vector>

#include "hex_utils.h"

struct patch_data
{
    patch_data(const std::shared_ptr<std::vector<char> > & data, int id) : data_(data), id_(id) {}

    std::shared_ptr<std::vector<char> > data_;
    int id_;
};

// TODO Should wrap this by a class in order to reject patch data from being
// added if it overlaps existing patch data; name it direct_patch or somethin
// and include the file hash in it; see direct_patch in patch_file_parser.
using patch = std::map<std::istream::pos_type, patch_data>;

class patch_tree final
{
    public:
        patch_tree() = default;
        patch_tree(patch_tree &&) = default;
        ~patch_tree() = default;

        bool add_patch(const array_of_bytes & aob, std::vector<char> && data, std::istream::off_type off = 0);

        std::optional<patch> generate_patch(std::istream & is) const;

    private:
        using patch_id = int;
        class aob_node;
        using node = std::variant<aob_node, patch_id>;
        struct aob_node
        {
            // TODO Store shortest depth from current node to cut short near end of file searches
            std::map<char, node> next_;
            // Note that this node must be an aob_node, but to keep the
            // add_patch code a bit simpler we also represent it as a node.
            std::unique_ptr<node> wildcard_;

            bool empty() const { return !wildcard_ && next_.empty(); }
        };

        std::map<char, node> roots_;

        struct raw_patch
        {
            std::istream::off_type off_;
            std::shared_ptr<std::vector<char> > data_;
        };

        std::vector<raw_patch> patches_;

        node & next_node(const std::optional<char> & b, aob_node & n);
        std::set<patch_id> find_matches(const node & n, std::istream & is) const;
        void find_matches(const node & n, std::istream & is, std::set<patch_id> & matches) const;
};

#endif
