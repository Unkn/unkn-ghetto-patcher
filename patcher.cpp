/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "patcher.h"

#include <fstream>

#include "Logger.h"

patcher::~patcher()
{
}

patcher::patcher(
        const std::string & file,
        const std::string & gen_revert,
        const std::string & gen_direct) :
    file_(file),
    gen_revert_(gen_revert),
    gen_direct_(gen_direct)
{
}

bool patcher::apply_patch(const patch & in) const
{
    std::fstream file(file_, std::ios_base::out | std::ios_base::in | std::ios_base::binary);
    return apply_patch_internal(in, file);
}

bool patcher::apply_patch(const patch_tree & pt) const
{
    std::fstream file(file_, std::ios_base::out | std::ios_base::in | std::ios_base::binary);
    auto op = pt.generate_patch(file);
    if (!op)
    {
        LOG_ERROR("Failed to generate patch");
        return false;
    }
    return apply_patch_internal(op.value(), file);
}

bool patcher::apply_patch_internal(const patch & in, std::fstream & file) const
{
    for (auto & p : in)
    {
        auto & raw = *p.second.data_;
        file.seekp(p.first);
        file.write(raw.data(), raw.size());
        LOG_NOTICE("0x{:08X}: {}", p.first, p.second.id_);
    }

    // TODO Failure if patch exceeds the size of the file
    LOG_INFO("Successfully completed patch");
    return true;
}
