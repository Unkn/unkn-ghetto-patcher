/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PATCHER_H
#define PATCHER_H

#include <string>

#include "patch_tree.h"

class patcher
{
    public:
        virtual ~patcher();

        virtual bool run() = 0;

    protected:
        std::string file_;
        // TODO Include a file hash in the top of patch files that are
        // directly addressed to warn or quit when the patched file doesnt
        // match
        std::string gen_revert_;
        std::string gen_direct_;

        patcher(
                const std::string & file,
                const std::string & gen_revert,
                const std::string & gen_direct);

        // TODO Change to direct_patch, see note in patch_tree
        bool apply_patch(const patch & in) const;
        bool apply_patch(const patch_tree & pt) const;

    private:
        bool apply_patch_internal(const patch & in, std::fstream & file) const;
};

#endif
