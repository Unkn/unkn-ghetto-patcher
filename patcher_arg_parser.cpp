/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "patcher_arg_parser.h"

#include <cassert>

#include "Logger.h"
#include "single_patcher.h"
#include "file_patcher.h"

patcher_arg_parser::patcher_arg_parser() :
    // TODO Add top usage lines to ArgParser for "prog args ....... description"
    // for more specific use cases since this program has a few
    file_('f', "file", "File to be patched"),
    aob_('a', "aob", "Array of bytes in the form 'XX XX ... XX' where 'XX' is either a hex byte or a wildcard (? or ?\?)"),
    patch_('p', "patch", "Patch to be applied at the address found"),
    offset_('o', "offset", "Offset from the location identified by the aob to apply patch. Default 0.", 0),
    // TODO Allow several patches to be loaded from a single file where they
    // are only applied if each specified patch has exactly 1 match and there
    // are no overlaps
    gen_revert_('r', "revert-output", "Output file for a reversion to the supplied patch"),
    gen_direct_('d', "direct-output", "Output file for a direct patch for the given file of the supplied patch"),
    load_('l', "load", "Apply patch from file"),
    patcher_()
{
    addOpt(file_);
    addOpt(aob_);
    addOpt(patch_);
    addOpt(offset_);
    addOpt(gen_revert_);
    addOpt(gen_direct_);
    addOpt(load_);
}

patcher_arg_parser::~patcher_arg_parser()
{
}

std::unique_ptr<patcher> patcher_arg_parser::parse(int argc, char **argv)
{
    ArgParser::parse(argc, argv);
    // Just copy it, maybe allow it to be moved. The parser itself wont exist
    // beyond the program initialization
    return std::unique_ptr<patcher>(patcher_.release());
}

bool patcher_arg_parser::validate()
{
    // File is always required
    if (!file_.parsed())
    {
        LOG_ERROR("File to be patched is required to be specified");
        return false;
    }

    // TODO Think of a better way to do this... could probably split the
    // validation between the patchers

#define SINGLE_PATCH    0x01
#define FILE_PATCH      0x02

    int method = 0;

    // Or together the bit flags representing the methods that the provided
    // args satisfy, then verify sufficient args have been supplied satisfying
    // the requirements for exactly one method.
    if ((aob_.parsed() || offset_.parsed()) && patch_.parsed())
        method |= SINGLE_PATCH;
    if (load_.parsed())
        method |= FILE_PATCH;

    if (method == SINGLE_PATCH)
    {
        // TODO Allow 2 use cases for offset:
        // - Offset from the address found by the aob match
        // - Offset from the beginning of file (no aob supplied)
        patcher_.reset(new single_patcher(
                    file_.file(),
                    gen_revert_.file(),
                    gen_direct_.file(),
                    aob_.aob(),
                    patch_.bin(),
                    offset_.value()
                    ));

    }
    else if (method == FILE_PATCH)
    {
        patcher_.reset(new file_patcher(
                    file_.file(),
                    gen_revert_.file(),
                    gen_direct_.file(),
                    load_.file()
                    ));
    }

    return !!patcher_;
}
