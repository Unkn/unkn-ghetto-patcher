/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef PATCHER_ARG_PARSER_H
#define PATCHER_ARG_PARSER_H

#include <variant>
#include <optional>

#include "ArgParser.h"
#include "aob_arg_opt.h"
#include "hex_arg_opt.h"
#include "file_arg_opt.h"
#include "primitive_arg_opt.h"
#include "patcher.h"

class patcher_arg_parser final : private ArgParser
{
    public:
        patcher_arg_parser();
        virtual ~patcher_arg_parser();

        std::unique_ptr<patcher> parse(int argc, char **argv);

    protected:
        bool validate() override;

    private:
        file_arg_opt file_;
        aob_arg_opt aob_;
        hex_arg_opt patch_;
        primitive_arg_opt<int> offset_;
        file_arg_opt gen_revert_;
        file_arg_opt gen_direct_;
        file_arg_opt load_;

        std::unique_ptr<patcher> patcher_;
};

#endif
