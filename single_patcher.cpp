/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "single_patcher.h"

#include <fstream>

#include "Logger.h"

single_patcher::single_patcher(
        const std::string & file,
        const std::string & gen_revert,
        const std::string & gen_direct,
        const array_of_bytes & aob,
        const std::vector<char> & patch,
        const int & offset) :
    patcher(file, gen_revert, gen_direct),
    aob_(aob),
    patch_(patch),
    offset_(offset)
{
}

single_patcher::~single_patcher()
{
}

bool single_patcher::run()
{
    bool ret = false;
    if (aob_.empty())
    {
        // TODO Verify offset >= 0
        patch p;
        p.emplace(std::piecewise_construct,
                std::forward_as_tuple(offset_),
                std::forward_as_tuple(std::make_shared<std::vector<char> >(std::move(patch_)), 1));
        if (!apply_patch(p))
            LOG_ERROR("Failed to apply single patch by offset");
        else
            ret = true;
    }
    else
    {
        patch_tree pt;
        if (!pt.add_patch(aob_, std::move(patch_), offset_))
            LOG_ERROR("Invalid patch input");
        else if (!apply_patch(pt))
            LOG_ERROR("Failed to apply single patch by aob");
        else
            ret = true;
    }
    return ret;
};
