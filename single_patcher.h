/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef SINGLE_PATCHER_H
#define SINGLE_PATCHER_H

#include "patcher.h"

class single_patcher final : public patcher
{
    public:
        single_patcher(
                const std::string & file,
                const std::string & gen_revert,
                const std::string & gen_direct,
                const array_of_bytes & aob,
                const std::vector<char> & patch,
                const int & offset);
        virtual ~single_patcher();

        bool run() override;

    private:
        array_of_bytes aob_;
        std::vector<char> patch_;
        int offset_;
};

#endif
