/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef STREAM_PARSER_H
#define STREAM_PARSER_H

#include <istream>

class stream_parser
{
    public:
        virtual ~stream_parser() { }

    protected:
        // To expose these to public scope in a child:
        //
        //    using stream_parser::read;
        //    using stream_parser::write;
        //
        // Only the implemented functions should be exposed.
        virtual bool read(std::istream &) { return false; }
        virtual bool write(std::ostream &) { return false; }
};

#endif
