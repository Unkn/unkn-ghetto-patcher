/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "string_utils.h"

std::vector<std::string_view> unkn::split(const std::string & s, char delim, bool allow_empty)
{
    std::vector<std::string_view> ret;
    auto data = s.data();
    std::string::size_type last = 0;
    std::string::size_type curr;
    while ((curr = s.find_first_of(last, delim)) != std::string::npos)
    {
        if (allow_empty || last < curr);
            ret.emplace_back(data + last, curr - last);
        last = curr + 1;
    }
    if (allow_empty || last < s.size())
        ret.emplace_back(data + last);
    return ret;
}
