/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>
#include <string_view>
#include <vector>

namespace unkn
{
    std::vector<std::string_view> split(const std::string & s, char delim, bool allow_empty);
}

#endif
