/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include "gtest/gtest.h"

#include "hex_utils.h"

// *****************************************************************************
// input_to_aob
// *****************************************************************************

static const std::optional<char> wild;

struct aob_test_vector
{
    aob_test_vector(std::string in, array_of_bytes out) : in_(in), out_(out) {}

    std::string in_;
    array_of_bytes out_;
} aob_s_tv[] =
{
    // Wildcards
    { "??" ,            { wild } },
    { "?" ,             { wild } },
    // Hex input not case sensitive
    { "AA",             { 0xAA } },
    { "aa",             { 0xAA } },
    { "aA",             { 0xAA } },
    { "Aa",             { 0xAA } },
    // Mixing hex and wildcards
    { "FA ?? FA ??",    { 0xFA, wild, 0xFA, wild } },
    { "?? FA ?? FA",    { wild, 0xFA, wild, 0xFA } },
    { "FA ? FA ?",      { 0xFA, wild, 0xFA, wild } },
    { "? FA ? FA",      { wild, 0xFA, wild, 0xFA } },
    { "? ??",           { wild, wild } },
    { "?? ?",           { wild, wild } },
};

class input_to_aob_success_tests : public testing::TestWithParam<aob_test_vector> {};

TEST_P(input_to_aob_success_tests, )
{
    auto & p = GetParam();
    auto aob = unkn::input_to_aob(p.in_);
    // All successful parses will yield a non-empty result
    ASSERT_FALSE(aob.empty());
    // Verify the parsed AOB is in fact what we expect it to be
    ASSERT_EQ(aob, p.out_);
}

INSTANTIATE_TEST_CASE_P(, input_to_aob_success_tests, testing::ValuesIn(aob_s_tv));

class input_to_aob_failure_tests : public testing::TestWithParam<std::string> {};

TEST_P(input_to_aob_failure_tests, )
{
    // All aob parse failures will yield an empty result
    ASSERT_TRUE(unkn::input_to_aob(GetParam()).empty());
}

INSTANTIATE_TEST_CASE_P(, input_to_aob_failure_tests, testing::Values(
            "",
            " ",
            "A",
            "A A",
            "AA A",
            "? A",
            "ax"
            ));

// *****************************************************************************
// hex_to_bin
// *****************************************************************************

struct hex_test_vector
{
    std::string hex_;
    std::vector<char> bin_;
} hex_s_tv[] =
{
    { "AA", { (char)0xAA } },
    { "aa", { (char)0xAA } },
    { "aA", { (char)0xAA } },
    { "Aa", { (char)0xAA } },
    { "44BB0011", { (char)0x44, (char)0xBB, (char)0x00, (char)0x11 } },
    { "44 BB 00 11", { (char)0x44, (char)0xBB, (char)0x00, (char)0x11 } }
};

class hex_to_bin_success_tests : public testing::TestWithParam<hex_test_vector> {};

TEST_P(hex_to_bin_success_tests, )
{
    auto & p = GetParam();
    auto bin = unkn::hex_to_bin(p.hex_);
    // All successful parses will yield a non-empty result
    ASSERT_FALSE(bin.empty());
    // Verify the parsed AOB is in fact what we expect it to be
    ASSERT_EQ(bin, p.bin_);
}

INSTANTIATE_TEST_CASE_P(, hex_to_bin_success_tests, testing::ValuesIn(hex_s_tv));

class hex_to_bin_failure_tests : public testing::TestWithParam<std::string> {};

TEST_P(hex_to_bin_failure_tests, )
{
    // All aob parse failures will yield an empty result
    ASSERT_TRUE(unkn::hex_to_bin(GetParam()).empty());
}

INSTANTIATE_TEST_CASE_P(, hex_to_bin_failure_tests, testing::Values(
            "",
            " ",
            "A",
            "A A",
            "AA A",
            "ax"
            ));
