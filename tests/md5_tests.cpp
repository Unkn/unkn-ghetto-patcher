/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <sstream>

#include "gtest/gtest.h"

#include "md5.h"
#include "hex_utils.h"

struct test_vector
{
    test_vector(std::string hash, std::string in) : hash_(unkn::hex_to_bin(hash)), in_(in) {}

    std::vector<char> hash_;
    std::string in_;
} tv[] =
{
    { "d41d8cd98f00b204e9800998ecf8427e", "" },
    { "9e107d9d372bb6826bd81d3542a419d6", "The quick brown fox jumps over the lazy dog" }
};

class md5_tests : public testing::TestWithParam<test_vector> {};

TEST_P(md5_tests, )
{
    auto & p = GetParam();
    std::stringstream ss(p.in_);
    auto hash = md5(ss);
    ASSERT_EQ(16, hash.size());
    ASSERT_EQ(16, p.hash_.size());
    ASSERT_TRUE(std::equal(
                hash.begin(), hash.end(),
                p.hash_.begin(), p.hash_.end(),
                [](unsigned char uc, char c) { return uc == reinterpret_cast<unsigned char&>(c); }));
}

INSTANTIATE_TEST_CASE_P(, md5_tests, testing::ValuesIn(tv));
